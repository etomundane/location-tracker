package com.sebangsa.mapstesting.di;

import android.app.Application;

import com.sebangsa.mapstesting.MapsApplication;
import com.sebangsa.mapstesting.di.module.AppModule;
import com.sebangsa.mapstesting.di.qualifier.ApplicationScope;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;

@ApplicationScope
@Component(modules = {AppModule.class})
public interface AppComponent extends AndroidInjector<MapsApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}