package com.sebangsa.mapstesting.di.module;

import com.sebangsa.mapstesting.data.local.LocalDataModule;
import com.sebangsa.mapstesting.data.remote.RemoteDataModule;
import com.sebangsa.mapstesting.data.repository.fcm.FCMRepoFactory;
import com.sebangsa.mapstesting.data.repository.fcm.FCMRepository;
import com.sebangsa.mapstesting.data.repository.liveevent.LiveEventRepoFactory;
import com.sebangsa.mapstesting.data.repository.liveevent.LiveEventRepository;
import com.sebangsa.mapstesting.data.repository.userlocation.UserLocationRepoFactory;
import com.sebangsa.mapstesting.data.repository.userlocation.UserLocationRepository;

import dagger.Binds;
import dagger.Module;

@Module(includes = {
        LocalDataModule.class,
        RemoteDataModule.class})
public abstract class DataModule {
    @Binds
    abstract FCMRepository fcmRepository(FCMRepoFactory fcmRepoFactory);

    @Binds
    abstract LiveEventRepository notificationRepository(LiveEventRepoFactory notificationData);

    @Binds
    abstract UserLocationRepository userLocationRepository(UserLocationRepoFactory userLocationRepoFactory);

}
