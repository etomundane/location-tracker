package com.sebangsa.mapstesting.di.module;

import android.app.Application;
import android.content.Context;

import com.sebangsa.mapstesting.di.qualifier.ApplicationContext;
import com.sebangsa.mapstesting.di.qualifier.ApplicationScope;

import dagger.Binds;
import dagger.Module;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.worker.AndroidWorkerInjectionModule;

@Module(includes = {
        AndroidSupportInjectionModule.class,
        AndroidWorkerInjectionModule.class,
        ActivityBuilder.class,
        DataModule.class,
        ServiceBuilder.class,
        UtilModule.class
})
public abstract class AppModule {
    @ApplicationScope
    @ApplicationContext
    @Binds
    abstract Context context(Application application);

}