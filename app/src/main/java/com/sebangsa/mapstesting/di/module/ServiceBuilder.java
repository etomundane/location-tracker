package com.sebangsa.mapstesting.di.module;

import com.sebangsa.mapstesting.service.firebase.FireBaseMessaging;
import com.sebangsa.mapstesting.service.locationtracker.LocationMeasurer;
import com.sebangsa.mapstesting.service.locationtracker.LocationTracker;
import com.sebangsa.mapstesting.service.workermanager.WorkerManagerModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module(includes = WorkerManagerModule.class)
public abstract class ServiceBuilder {

    @ContributesAndroidInjector
    abstract FireBaseMessaging fireBaseMessaging();

    @ContributesAndroidInjector
    abstract LocationTracker locationTracker();

    @ContributesAndroidInjector
    abstract LocationMeasurer locationMeasurer();
}
