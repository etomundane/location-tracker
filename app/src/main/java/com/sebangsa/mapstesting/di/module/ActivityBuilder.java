package com.sebangsa.mapstesting.di.module;

import com.sebangsa.mapstesting.ui.home.MapsActivity;
import com.sebangsa.mapstesting.ui.TomatoesActivity;
import com.sebangsa.mapstesting.di.qualifier.ActivityScope;
import com.sebangsa.mapstesting.ui.home.MapsModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = MapsModule.class)
    abstract MapsActivity mapsActivity();

    @ActivityScope
    @ContributesAndroidInjector
    abstract TomatoesActivity tomatoesActivity();
}
