package com.sebangsa.mapstesting.di.module;

import com.sebangsa.mapstesting.di.qualifier.ApplicationScope;
import com.sebangsa.mapstesting.util.RxScheduler;
import com.sebangsa.mapstesting.util.RxSchedulerProvider;

import dagger.Binds;
import dagger.Module;

@Module
abstract class UtilModule {

    @ApplicationScope
    @Binds
    abstract RxScheduler rxScheduler(RxSchedulerProvider rxSchedulerProvider);

}
