package com.sebangsa.mapstesting.ui;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sebangsa.mapstesting.R;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;
import com.sebangsa.mapstesting.di.qualifier.DummyLiveEvents;
import com.sebangsa.mapstesting.ui.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class TomatoesActivity extends BaseActivity implements OnMapReadyCallback {

    private PopupMenu popup;
    private GoogleMap gMap;
    private BitmapDescriptor eventMarker;
    private GoogleMap.SnapshotReadyCallback snapshotCallback;

    private GridLayout vPinContainer;

    @Inject
    @DummyLiveEvents
    protected List<LiveEventLocation> nearbyLocations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tomatoes);
        findViewById(R.id.v_back).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.iv_options).setOnClickListener(this::showEventOptions);
        vPinContainer = findViewById(R.id.pin_container);
        eventMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_event_marker);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.v_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    private void showEventOptions(View v) {
        if (popup == null) {
            popup = new PopupMenu(this, v);
            popup.getMenuInflater().inflate(R.menu.menu_options_event_detail, popup.getMenu());
            popup.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.menu_forum:
                        Toast.makeText(TomatoesActivity.this, "Forum", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.menu_leader_board:
                        Toast.makeText(TomatoesActivity.this, "Peringkat", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
                return false;
            });
        }
        popup.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        try {
            gMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.gmap_style));
        } catch (Resources.NotFoundException e) {
            Timber.e(e);
        }
        loopMapSnapshot(0);
    }

    private void loopMapSnapshot(int index) {
        if (index == nearbyLocations.size()) {
            return;
        }
        LiveEventLocation location = nearbyLocations.get(index);
        if (gMap == null) {
            return;
        }
        gMap.clear();
        gMap.setOnMapLoadedCallback(null);
        gMap.setMyLocationEnabled(false);
        gMap.getUiSettings().setMyLocationButtonEnabled(false);

        LatLng coordinate = new LatLng(location.getLat(), location.getLng());
        MarkerOptions marker = new MarkerOptions().position(coordinate).anchor(0.5f, 0.5f).title(location.getName()).icon(eventMarker);
        gMap.addMarker(marker);
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 14.5f));
        gMap.setOnMapLoadedCallback(() -> {
            snapshotCallback = bitmap -> {
                Timber.e("SNAPSHOT: %s", bitmap.getGenerationId());
                ImageView v = new ImageView(this);
                v.setImageBitmap(bitmap);
                vPinContainer.addView(v, index);
                if (index < nearbyLocations.size()) {
                    loopMapSnapshot(index + 1);
                }
            };
            gMap.snapshot(snapshotCallback);
        });
    }
}
