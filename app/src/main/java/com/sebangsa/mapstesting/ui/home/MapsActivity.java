package com.sebangsa.mapstesting.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sebangsa.mapstesting.R;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;
import com.sebangsa.mapstesting.di.qualifier.DummyLiveEvents;
import com.sebangsa.mapstesting.service.workermanager.AppWorker;
import com.sebangsa.mapstesting.ui.base.BaseActivity;
import com.sebangsa.mapstesting.util.RxScheduler;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.core.app.ActivityCompat;
import timber.log.Timber;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback {
    private SupportMapFragment mapFragment;

    @Inject
    protected AppWorker appWorker;

    @Inject
    @DummyLiveEvents
    protected List<LiveEventLocation> nearbyLocations;

    @Inject
    RxScheduler scheduler;

    private ImageView vBgPlaceDesc;
    private TextView vStatus;
    private FloatingActionButton vForum;
    private FloatingActionButton vLeaderBoard;
    private FloatingActionButton vMyLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        vStatus = findViewById(R.id.v_status);
        vBgPlaceDesc = findViewById(R.id.iv_bg_place_desc);
        vForum = findViewById(R.id.fab_forum);
        vLeaderBoard = findViewById(R.id.fab_leader_board);
        vMyLocation = findViewById(R.id.fab_my_location);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        //            vStatus.setActivated(!vStatus.isActivated());
        //            vStatus.setText(vStatus.isActivated() ? getString(R.string.title_live) : getString(R.string.title_upcoming));
        vForum.setOnClickListener(this::showPopUp);

        vLeaderBoard.setOnClickListener(v -> {
            vStatus.setEnabled(false);
            vStatus.setText(getString(R.string.status_ended));
            vBgPlaceDesc.setImageResource(R.drawable.bg_place_desc_ended);
            showPlaceDesc(false);
        });

        View anchor = findViewById(R.id.v_menu_anchor);
        (findViewById(R.id.v_close_toast_upcoming)).setOnClickListener(v -> showPopUp(anchor));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mapFragment != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    mapFragment.getMapAsync(this);
                    appWorker.startLocationTracker();
                }
            } else {
                mapFragment.getMapAsync(this);
                appWorker.startLocationTracker();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mapFragment.getMapAsync(this);
                appWorker.startLocationTracker();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        try {
            boolean success = gMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.gmap_style));
            if (!success) {
                Timber.e("Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Timber.e(e);
        }
        gMap.clear();
        vMyLocation.setOnClickListener(null);
        gMap.setMyLocationEnabled(true);
        gMap.getUiSettings().setMyLocationButtonEnabled(false);


        BitmapDescriptor eventMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_event_marker);
        LatLngBounds.Builder boundBuilder = new LatLngBounds.Builder();

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            @SuppressLint("MissingPermission") Location myLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));
            if (myLocation != null) {
                boundBuilder.include(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
            }
        }

        for (LiveEventLocation location : nearbyLocations) {
            LatLng coordinate = new LatLng(location.getLat(), location.getLng());
            MarkerOptions marker = new MarkerOptions().position(coordinate).anchor(0.5f, 0.5f).title(location.getName()).icon(eventMarker);

            gMap.addMarker(marker);
            gMap.addCircle(getEventRadius1(coordinate, location.getRadius()));
            gMap.addCircle(getEventRadius2(coordinate, location.getRadius()));
            boundBuilder.include(coordinate);
        }

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(boundBuilder.build(), getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels, 120);
        gMap.animateCamera(cu);

        showPlaceDesc(true);
        vMyLocation.setOnClickListener(v -> gMap.animateCamera(cu));
    }

    private void showPlaceDesc(boolean isLive) {
//        Glide.with(this)
//                .load(isLive ? R.drawable.bg_place_desc : R.drawable.bg_place_desc_ended)
//                .fitCenter()
//                .apply(RequestOptions.bitmapTransform(new RoundedCorners(32)))
//                .into(vBgPlaceDesc);

        findViewById(R.id.v_play).setVisibility(isLive ? View.VISIBLE : View.GONE);
    }

    private CircleOptions getEventRadius1(LatLng centerPoint, double radius) {
        return new CircleOptions()
                .radius(radius)
                .strokeWidth(0)
                .fillColor(Color.parseColor("#22ff0000"))
                .center(centerPoint);
    }

    private CircleOptions getEventRadius2(LatLng centerPoint, double radius) {
        return new CircleOptions()
                .radius(radius * 0.75)
                .strokeWidth(0)
                .fillColor(Color.parseColor("#22ff0000"))
                .center(centerPoint);
    }

    private void showPopUp(View anchor) {
        MenuBuilder menuBuilder =new MenuBuilder(this);
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.menu_options_event_detail, menuBuilder);
        MenuPopupHelper optionsMenu = new MenuPopupHelper(this, menuBuilder, anchor);
        optionsMenu.setForceShowIcon(true);
        optionsMenu.setGravity(Gravity.END);

        menuBuilder.setCallback(new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_forum:
                        Toast.makeText(MapsActivity.this, "Forum", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.menu_leader_board:
                        Toast.makeText(MapsActivity.this, "Leader Board", Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {}
        });

        optionsMenu.show();
    }
}
