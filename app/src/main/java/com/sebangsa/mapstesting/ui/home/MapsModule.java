package com.sebangsa.mapstesting.ui.home;

import android.content.Context;

import com.sebangsa.mapstesting.ui.home.MapsActivity;
import com.sebangsa.mapstesting.di.qualifier.ActivityContext;
import com.sebangsa.mapstesting.di.qualifier.ActivityScope;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MapsModule {
    @ActivityScope
    @ActivityContext
    @Binds
    abstract Context context(MapsActivity context);

}
