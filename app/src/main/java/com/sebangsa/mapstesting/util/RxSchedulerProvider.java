package com.sebangsa.mapstesting.util;

import javax.inject.Inject;

import io.reactivex.CompletableTransformer;
import io.reactivex.FlowableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RxSchedulerProvider implements RxScheduler {

    @Inject
    public RxSchedulerProvider() {
    }

    @Override
    public Scheduler io() {
        return Schedulers.io();
    }

    @Override
    public Scheduler computation() {
        return Schedulers.computation();
    }

    @Override
    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }

    @Override
    public <T> ObservableTransformer<T, T> observableIo() {
        return upstream -> upstream.subscribeOn(io());
    }

    @Override
    public <T> ObservableTransformer<T, T> observableIoUi() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public <T> ObservableTransformer<T, T> observableComputationUi() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }

    @Override
    public <T> SingleTransformer<T, T> singleIo() {
        return upstream -> upstream.subscribeOn(io());
    }

    @Override
    public <T> SingleTransformer<T, T> singleIoUi() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public <T> SingleTransformer<T, T> singleComputationUi() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }

    @Override
    public <T> FlowableTransformer<T, T> flowableIo() {
        return upstream -> upstream.subscribeOn(io());
    }

    @Override
    public <T> FlowableTransformer<T, T> flowableIoUi() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public <T> FlowableTransformer<T, T> flowableComputationUi() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }

    @Override
    public <T> MaybeTransformer<T, T> maybeIo() {
        return upstream -> upstream.subscribeOn(io());
    }

    @Override
    public <T> MaybeTransformer<T, T> maybeIoUi() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public <T> MaybeTransformer<T, T> maybeComputationUi() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }

    @Override
    public CompletableTransformer completableIo() {
        return upstream -> upstream.subscribeOn(io());
    }

    @Override
    public CompletableTransformer completableIoUi() {
        return upstream -> upstream.subscribeOn(io()).observeOn(ui());
    }

    @Override
    public CompletableTransformer completableComputationUi() {
        return upstream -> upstream.subscribeOn(computation()).observeOn(ui());
    }
}
