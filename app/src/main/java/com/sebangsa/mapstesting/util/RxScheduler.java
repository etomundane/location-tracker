package com.sebangsa.mapstesting.util;

import io.reactivex.CompletableTransformer;
import io.reactivex.FlowableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;

public interface RxScheduler {
    Scheduler io();

    Scheduler computation();

    Scheduler ui();


    <T> ObservableTransformer<T, T> observableIo();

    <T> ObservableTransformer<T, T> observableIoUi();

    <T> ObservableTransformer<T, T> observableComputationUi();

    <T> SingleTransformer<T, T> singleIo();

    <T> SingleTransformer<T, T> singleIoUi();

    <T> SingleTransformer<T, T> singleComputationUi();

    <T> FlowableTransformer<T, T> flowableIo();

    <T> FlowableTransformer<T, T> flowableIoUi();

    <T> FlowableTransformer<T, T> flowableComputationUi();

    <T> MaybeTransformer<T, T> maybeIo();

    <T> MaybeTransformer<T, T> maybeIoUi();

    <T> MaybeTransformer<T, T> maybeComputationUi();

    CompletableTransformer completableIo();

    CompletableTransformer completableIoUi();

    CompletableTransformer completableComputationUi();
}
