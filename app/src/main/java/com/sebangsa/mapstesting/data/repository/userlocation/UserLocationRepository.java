package com.sebangsa.mapstesting.data.repository.userlocation;

import android.location.Location;

public interface UserLocationRepository {
    Location getLastUserLocation();

    void setUserLocation(double lat, double lng);

    long getLastLocationSavedTime();
}
