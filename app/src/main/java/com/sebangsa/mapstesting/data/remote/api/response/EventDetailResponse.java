package com.sebangsa.mapstesting.data.remote.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class EventDetailResponse {
    @SerializedName("bonus_daya")
    @Expose
    private long bonusDaya;
    @SerializedName("date_end")
    @Expose
    private long dateEnd;
    @SerializedName("date_release")
    @Expose
    private long dateRelease;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("event_status")
    @Expose
    private long eventStatus;
    @SerializedName("gift")
    @Expose
    private List<Gift> gift = null;
    @SerializedName("got_gift")
    @Expose
    private boolean gotGift;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("image_icon")
    @Expose
    private String imageIcon;
    @SerializedName("image_thumb")
    @Expose
    private String imageThumb;
    @SerializedName("is_expired")
    @Expose
    private boolean isExpired;
    @SerializedName("is_festival")
    @Expose
    private boolean isFestival;
    @SerializedName("is_join")
    @Expose
    private boolean isJoin;
    @SerializedName("location")
    @Expose
    private EventLocation location;
    @SerializedName("partisipan")
    @Expose
    private long partisipan;
    @SerializedName("status")
    @Expose
    private long status;
    @SerializedName("tantangan")
    @Expose
    private List<Tantangan> tantangan = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("user")
    @Expose
    private User user;

    @Data
    private class Avatar {

        @SerializedName("large")
        @Expose
        private String large;
        @SerializedName("medium")
        @Expose
        private String medium;
        @SerializedName("small")
        @Expose
        private String small;

    }

    @Data
    private class Choice {

        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("id")
        @Expose
        private long id;
        @SerializedName("is_voted")
        @Expose
        private boolean isVoted;
        @SerializedName("percentage")
        @Expose
        private long percentage;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("total_vote")
        @Expose
        private long totalVote;

    }

    @Data
    private class Clue {

        @SerializedName("comment")
        @Expose
        private Comment comment;
        @SerializedName("full")
        @Expose
        private Full full;
        @SerializedName("grid")
        @Expose
        private Grid grid;
        @SerializedName("list")
        @Expose
        private ClueList clueList;
        @SerializedName("suggested")
        @Expose
        private Suggested suggested;

    }

    @Data
    private class Comment {

        @SerializedName("height")
        @Expose
        private long height;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("width")
        @Expose
        private long width;

    }

    @Data
    private class Full {

        @SerializedName("height")
        @Expose
        private long height;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("width")
        @Expose
        private long width;

    }

    @Data
    private class Gift {

        @SerializedName("daya_get")
        @Expose
        private long dayaGet;
        @SerializedName("id")
        @Expose
        private long id;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("image_thumb")
        @Expose
        private String imageThumb;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("type")
        @Expose
        private String type;

    }

    @Data
    private class Grid {

        @SerializedName("height")
        @Expose
        private long height;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("width")
        @Expose
        private long width;

    }

    @Data
    private class ClueList {

        @SerializedName("height")
        @Expose
        private long height;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("width")
        @Expose
        private long width;

    }

    @Data
    private class Suggested {

        @SerializedName("height")
        @Expose
        private long height;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("width")
        @Expose
        private long width;

    }

    @Data
    private class Tag {

        @SerializedName("id")
        @Expose
        private long id;
        @SerializedName("text")
        @Expose
        private String text;

    }

    @Data
    private class Tantangan {

        @SerializedName("choices")
        @Expose
        private List<Choice> choices = null;
        @SerializedName("clue")
        @Expose
        private Clue clue;
        @SerializedName("clue_type")
        @Expose
        private String clueType;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("fact")
        @Expose
        private String fact;
        @SerializedName("id")
        @Expose
        private long id;
        @SerializedName("is_voted")
        @Expose
        private boolean isVoted;
        @SerializedName("tags")
        @Expose
        private List<Tag> tags = null;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("type")
        @Expose
        private long type;

    }

    @Data
    private class User {

        @SerializedName("activity")
        @Expose
        private long activity;
        @SerializedName("avatar")
        @Expose
        private Avatar avatar;
        @SerializedName("guard_active")
        @Expose
        private boolean guardActive;
        @SerializedName("guard_expired")
        @Expose
        private long guardExpired;
        @SerializedName("guard_time_left")
        @Expose
        private long guardTimeLeft;
        @SerializedName("id")
        @Expose
        private long id;
        @SerializedName("phone_number")
        @Expose
        private String phoneNumber;
        @SerializedName("username")
        @Expose
        private String username;
    }

    @Data
    private class EventLocation {
        @SerializedName("id")
        @Expose
        private long id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("lat")
        @Expose
        private double lat;
        @SerializedName("lng")
        @Expose
        private double lng;
    }
}
