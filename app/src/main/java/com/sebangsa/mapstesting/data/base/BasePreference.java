package com.sebangsa.mapstesting.data.base;

import android.content.SharedPreferences;

public abstract class BasePreference {

    private SharedPreferences pref;

    protected BasePreference(SharedPreferences preferences) {
        this.pref = preferences;
    }

    private SharedPreferences.Editor edit() {
        return pref.edit();
    }

    protected boolean contains(String key) {
        return pref.contains(key);
    }

    protected void remove(String key) {
        edit().remove(key).apply();
    }

    protected void clear() {
        pref.edit().clear().apply();
    }


    /* GETTER */
    protected String get(String key, String fallback) {
        return pref.getString(key, fallback);
    }

    protected int get(String key, int fallback) {
        return pref.getInt(key, fallback);
    }

    protected long get(String key, long fallback) {
        return pref.getLong(key, fallback);
    }

    protected float get(String key, float fallback) {
        return pref.getFloat(key, fallback);
    }

    protected boolean get(String key, boolean fallback) {
        return pref.getBoolean(key, fallback);
    }


    /* SETTER */
    protected void set(String key, String value) {
        edit().putString(key, value).apply();
    }

    protected void set(String key, int value) {
        edit().putInt(key, value).apply();
    }

    protected void set(String key, long value) {
        edit().putLong(key, value).apply();
    }

    protected void set(String key, float value) {
        edit().putFloat(key, value).apply();
    }

    protected void set(String key, boolean value) {
        edit().putBoolean(key, value).apply();
    }

}
