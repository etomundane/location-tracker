package com.sebangsa.mapstesting.data.repository.liveevent;

import com.sebangsa.mapstesting.data.base.BaseRepository;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;
import com.sebangsa.mapstesting.di.qualifier.LocalData;
import com.sebangsa.mapstesting.di.qualifier.RemoteData;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

public class LiveEventRepoFactory extends BaseRepository<LiveEventSource> implements LiveEventRepository {

    @Inject
    public LiveEventRepoFactory(@LocalData LiveEventSource localData, @RemoteData LiveEventSource remoteData) {
        super(localData, remoteData);
    }

    @Override
    public Observable<List<LiveEventLocation>> getAllLiveEventLocation() {
//        Local data modification will be overwrite by fresh data from remote
        return Single.mergeDelayError(
                getRemoteData().getLiveEventLocations().doOnSuccess(it -> getLocalData().saveLiveEventLocations(it).subscribe()),
                getLocalData().getLiveEventLocations()).firstElement().toObservable();
    }

    @Override
    public Single<List<Long>> updateLiveEventLocations(List<LiveEventLocation> liveEventLocations) {
        return getLocalData().saveLiveEventLocations(liveEventLocations);
    }

}
