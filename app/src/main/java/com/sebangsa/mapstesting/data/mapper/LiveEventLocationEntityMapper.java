package com.sebangsa.mapstesting.data.mapper;

import com.sebangsa.mapstesting.data.base.DataMapper;
import com.sebangsa.mapstesting.data.local.database.entity.LiveEventLocationEntity;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;

import javax.inject.Inject;

public final class LiveEventLocationEntityMapper implements DataMapper<LiveEventLocationEntity, LiveEventLocation> {

    @Inject
    public LiveEventLocationEntityMapper() {
    }

    @Override
    public LiveEventLocationEntity fromData(LiveEventLocation data) {
        return new LiveEventLocationEntity(data.getId(), data.getName(), data.getLat(), data.getLng(), data.getRadius(), data.getLastNotified());
    }

    @Override
    public LiveEventLocation toData(LiveEventLocationEntity entity) {
        LiveEventLocation result = new LiveEventLocation();
        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setLat(entity.getLat());
        result.setLng(entity.getLng());
        result.setRadius(entity.getRadius());
        result.setLastNotified(entity.getLastNotified());
        return result;
    }
}
