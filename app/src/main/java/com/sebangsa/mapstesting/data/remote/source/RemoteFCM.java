package com.sebangsa.mapstesting.data.remote.source;

import com.sebangsa.mapstesting.data.repository.fcm.FCMSource;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;

public class RemoteFCM implements FCMSource {

    @Inject
    public RemoteFCM() {
    }

    @Override
    public Observable<String> updateRemoteToken(long uid, String token) {
        return null;
    }

    @Override
    public Completable saveLocalToken(String token) {
        return null;
    }

    @Override
    public Observable<String> getToken() {
        return null;
    }
}
