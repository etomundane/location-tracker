package com.sebangsa.mapstesting.data.repository.fcm;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface FCMRepository {
    Completable persistToken(String token);

    Observable<String> updateTokenForUser(long uid, String token);

    Observable<String> getFcmToken();

}
