package com.sebangsa.mapstesting.data.remote;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sebangsa.mapstesting.di.qualifier.ApplicationContext;
import com.sebangsa.mapstesting.di.qualifier.ApplicationScope;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = {
        ApiModule.class,
        RemoteDataSourceModule.class
})
public abstract class RemoteDataModule {

    @ApplicationScope
    @Provides
    static Gson gson() {
        return new GsonBuilder().setLenient().create();
    }

//    @ApplicationScope
//    @Provides
//    static Interceptor interceptor(@IsDebuggable boolean isDebuggable) {
//        return new HttpLoggingInterceptor().setLevel(isDebuggable ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
//    }

    @ApplicationScope
    @Provides
    static Cache cache(@ApplicationContext Context context) {
        return new Cache(context.getCacheDir(), 10 * 1024 * 1024);
    }

    @ApplicationScope
    @Provides
    static OkHttpClient okHttpClient(/*@LoggingInterceptor Interceptor loggingInterceptor*/) {
        return new OkHttpClient.Builder()
//                .addInterceptor(loggingInterceptor)
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .build();
    }

    @ApplicationScope
    @Provides
    static Retrofit retrofit(OkHttpClient client, Gson gson/*, @BaseUrl String baseUrl*/) {
        return new Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://api.dummy.net/")
                .build();
    }


}
