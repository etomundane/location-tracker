package com.sebangsa.mapstesting.data.remote;

import com.sebangsa.mapstesting.data.remote.source.RemoteFCM;
import com.sebangsa.mapstesting.data.remote.source.RemoteLiveEvent;
import com.sebangsa.mapstesting.data.repository.fcm.FCMSource;
import com.sebangsa.mapstesting.data.repository.liveevent.LiveEventSource;
import com.sebangsa.mapstesting.di.qualifier.RemoteData;

import dagger.Binds;
import dagger.Module;

@Module
abstract class RemoteDataSourceModule {
    @Binds
    @RemoteData
    abstract FCMSource fcmSource(RemoteFCM remoteFCM);

    @Binds
    @RemoteData
    abstract LiveEventSource liveEventSource(RemoteLiveEvent remoteLiveEvent);
}
