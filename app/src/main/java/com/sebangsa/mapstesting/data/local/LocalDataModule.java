package com.sebangsa.mapstesting.data.local;

import android.content.Context;

import com.sebangsa.mapstesting.data.local.preference.AppPreference;
import com.sebangsa.mapstesting.data.local.preference.FCMPreference;
import com.sebangsa.mapstesting.data.local.preference.LocationPreference;
import com.sebangsa.mapstesting.di.qualifier.ApplicationContext;
import com.sebangsa.mapstesting.di.qualifier.ApplicationScope;
import com.sebangsa.mapstesting.di.qualifier.DatabaseName;
import com.sebangsa.mapstesting.data.local.database.AppDatabase;

import androidx.room.Room;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module(includes = LocalDataSourceModule.class)
public abstract class LocalDataModule {

    @Provides
    @DatabaseName
    static String databaseName() {
        return AppDatabase.DB_NAME;
    }

    @ApplicationScope
    @Provides
    static AppDatabase appDatabase(@ApplicationContext Context context, @DatabaseName String dbName) {
        return Room.databaseBuilder(context, AppDatabase.class, dbName)
                .fallbackToDestructiveMigration()
                .build();
    }

    @Binds
    abstract AppPreference.FCMPreference fcmPreference(FCMPreference fcmPreference);

    @Binds
    abstract AppPreference.LocationPreference locationPreference(LocationPreference locationPreference);

}
