package com.sebangsa.mapstesting.data.model.entity;

import android.location.Location;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public final class LiveEventLocation {
    private long id;
    private String name;
    private double lat;
    private double lng;
    private double radius;
    private long lastNotified;

    public Location asAndroidLocation() {
        Location result = new Location(name);
        result.setLatitude(lat);
        result.setLongitude(lng);
        return result;
    }
}
