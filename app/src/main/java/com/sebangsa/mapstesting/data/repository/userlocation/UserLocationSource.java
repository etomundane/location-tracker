package com.sebangsa.mapstesting.data.repository.userlocation;

import android.location.Location;

public interface UserLocationSource {
    Location getLastUserLocation();

    void setUserLocation(double lat, double lng);

    long getLastLocationSavedTime();
}
