package com.sebangsa.mapstesting.data.repository.liveevent;

import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface LiveEventRepository {

    Observable<List<LiveEventLocation>> getAllLiveEventLocation();

    Single<List<Long>> updateLiveEventLocations(List<LiveEventLocation> liveEventLocations);
}
