package com.sebangsa.mapstesting.data.local.preference;

import android.content.Context;
import android.location.Location;

import com.sebangsa.mapstesting.di.qualifier.ApplicationContext;

import javax.inject.Inject;

public class LocationPreference extends AppPreference implements AppPreference.LocationPreference {
    private static final String LAST_LAT = "cmevNO9prcP8FhePPcwjQnustTfW66rJ";
    private static final String LAST_LNG = "EQR59IrsgohPIoSb8KNhAh43SR0NUU1S";
    private static final String LAST_SAVED_TIME = "nYmX3BD8OCcYmNe4HrRQquBLgi3jZu7x";

    private static final float DEFAULT_LAT = (float) -7.782903;
    private static final float DEFAULT_LNG = (float) 110.367050;

    @Inject
    protected LocationPreference(@ApplicationContext Context context) {
        super(context);
    }

    @Override
    public Location getLastKnownLocation() {
        Location lastLocation = new Location("My Location");
        lastLocation.setLatitude(get(LAST_LAT, DEFAULT_LAT));
        lastLocation.setLongitude(get(LAST_LNG, DEFAULT_LNG));
        return lastLocation;
    }

    @Override
    public void setLastKnownLocation(double lat, double lng) {
        set(LAST_LAT, (float) lat);
        set(LAST_LNG, (float) lng);
        set(LAST_SAVED_TIME, System.currentTimeMillis());
    }

    @Override
    public long getLastSavedTime() {
        return get(LAST_SAVED_TIME, -1L);
    }
}
