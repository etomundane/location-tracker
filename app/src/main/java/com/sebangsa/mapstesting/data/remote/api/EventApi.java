package com.sebangsa.mapstesting.data.remote.api;

import com.sebangsa.mapstesting.data.remote.api.response.EventDetailResponse;
import com.sebangsa.mapstesting.data.remote.api.response.LiveEventLocationResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface EventApi {

    @FormUrlEncoded
    @POST("event/{event_id}")
    Observable<EventDetailResponse> getEventDetail(
            @HeaderMap Map<String, String> nonAuthHeader,
            @Query("event_id") String eventId);

    @FormUrlEncoded
    @POST("register")
    Observable<List<LiveEventLocationResponse>> getLiveEventLocations(
            @HeaderMap Map<String, String> nonAuthHeader);

}
