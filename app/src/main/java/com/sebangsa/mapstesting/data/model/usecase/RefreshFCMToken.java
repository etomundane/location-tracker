package com.sebangsa.mapstesting.data.model.usecase;

import com.sebangsa.mapstesting.data.base.UseCase;
import com.sebangsa.mapstesting.data.repository.fcm.FCMRepository;
import com.sebangsa.mapstesting.util.RxScheduler;

import javax.inject.Inject;

public class RefreshFCMToken implements UseCase<Void, String> {
    private FCMRepository fcmRepository;
    private RxScheduler scheduler;

    @Inject
    public RefreshFCMToken(FCMRepository fcmRepository, RxScheduler scheduler) {
        this.fcmRepository = fcmRepository;
        this.scheduler = scheduler;
    }

    @Override
    public Void execute(String s) {
        /* TODO UPDATE TO BACK END */
        fcmRepository.persistToken(s)
                .compose(scheduler.completableIo())
                .subscribe();
        return null;
    }
}
