package com.sebangsa.mapstesting.data.base;

import java.util.List;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;
import io.reactivex.Completable;
import io.reactivex.Single;

public interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insert(T obj);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<List<Long>> insert(List<T> obj);


    @Update
    Single<Integer> update(T obj);

    @Update
    Completable update(List<T> listObj);

    @Delete
    Single<Integer> delete(T obj);

    @Delete
    Completable delete(List<T> listObj);

}
