package com.sebangsa.mapstesting.data.local.source;

import com.sebangsa.mapstesting.data.local.preference.AppPreference;
import com.sebangsa.mapstesting.data.repository.fcm.FCMSource;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;

public class LocalFCM implements FCMSource {
    private AppPreference.FCMPreference fireBasePreference;

    @Inject
    public LocalFCM(AppPreference.FCMPreference fireBasePreference) {
        this.fireBasePreference = fireBasePreference;
    }

    @Override
    public Observable<String> updateRemoteToken(long uid, String token) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Completable saveLocalToken(String token) {
        try {
            fireBasePreference.setFireBaseToken(token);
            return Completable.complete();
        } catch (Exception e) {
            return Completable.error(e);
        }
    }

    @Override
    public Observable<String> getToken() {
        return Observable.just(fireBasePreference.getFCMToken());
    }
}
