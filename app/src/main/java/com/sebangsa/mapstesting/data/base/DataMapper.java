package com.sebangsa.mapstesting.data.base;

public interface DataMapper<E, D> {
    E fromData(D data) throws Exception;

    D toData(E entity) throws Exception;
}
