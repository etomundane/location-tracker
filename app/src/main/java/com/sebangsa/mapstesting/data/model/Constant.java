package com.sebangsa.mapstesting.data.model;

public interface Constant {
    long MAX_LOCATION_CHECKING_INTERVAL = 5 * 60 * 1000; /* 5 minutes */
    long MIN_USER_MOVING_DISTANCE = 2; /* in meters */
}
