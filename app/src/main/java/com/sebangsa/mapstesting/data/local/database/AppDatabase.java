package com.sebangsa.mapstesting.data.local.database;

import com.sebangsa.mapstesting.data.local.database.dao.LiveEventLocationDao;
import com.sebangsa.mapstesting.data.local.database.entity.LiveEventLocationEntity;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(version = AppDatabase.DB_VERSION,
        exportSchema = false,
        entities = {LiveEventLocationEntity.class})
public abstract class AppDatabase extends RoomDatabase {
    public static final String DB_NAME = "pq5vPWRNckQfLrQkfXaxrwwwBBXt97Rd";
    static final int DB_VERSION = 1;

    public abstract LiveEventLocationDao liveEventLocationDao();
}
