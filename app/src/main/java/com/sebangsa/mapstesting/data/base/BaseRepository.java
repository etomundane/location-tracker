package com.sebangsa.mapstesting.data.base;

import lombok.Getter;

public abstract class BaseRepository<DATA_SOURCE> {
    @Getter
    private DATA_SOURCE localData;

    @Getter
    private DATA_SOURCE remoteData;

    public BaseRepository(DATA_SOURCE localData, DATA_SOURCE remoteData) {
        this.localData = localData;
        this.remoteData = remoteData;
    }

    public BaseRepository(DATA_SOURCE localData) {
        this.localData = localData;
    }
}
