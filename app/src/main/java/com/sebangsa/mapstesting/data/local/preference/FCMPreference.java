package com.sebangsa.mapstesting.data.local.preference;

import android.content.Context;

import com.sebangsa.mapstesting.di.qualifier.ApplicationContext;
import com.sebangsa.mapstesting.di.qualifier.ApplicationScope;

import javax.inject.Inject;

@ApplicationScope
public class FCMPreference extends AppPreference implements AppPreference.FCMPreference {

    private static final String FIRE_TOKEN = "HaxvoMsn3vDIJUQgo20nDnFFuuBKrq5b";

    @Inject
    FCMPreference(@ApplicationContext Context context) {
        super(context);
    }

    @Override
    public String getFCMToken() {
        return get(FIRE_TOKEN, "");
    }

    @Override
    public void setFireBaseToken(String token) {
        set(FIRE_TOKEN, token);
    }
}
