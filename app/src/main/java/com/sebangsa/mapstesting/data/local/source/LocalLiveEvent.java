package com.sebangsa.mapstesting.data.local.source;

import com.sebangsa.mapstesting.data.local.database.AppDatabase;
import com.sebangsa.mapstesting.data.local.database.dao.LiveEventLocationDao;
import com.sebangsa.mapstesting.data.local.database.entity.LiveEventLocationEntity;
import com.sebangsa.mapstesting.data.mapper.LiveEventLocationEntityMapper;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;
import com.sebangsa.mapstesting.data.repository.liveevent.LiveEventSource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

public class LocalLiveEvent implements LiveEventSource {
    private LiveEventLocationDao liveEventLocationDao;
    private LiveEventLocationEntityMapper liveEventLocationMapper;

    @Inject
    LocalLiveEvent(AppDatabase db, LiveEventLocationEntityMapper liveEventLocationMapper) {
        this.liveEventLocationDao = db.liveEventLocationDao();
        this.liveEventLocationMapper = liveEventLocationMapper;
    }

    @Override
    public Observable<LiveEventLocation> getLiveEventLocation(long id) {
        return liveEventLocationDao.get(id).map(it -> liveEventLocationMapper.toData(it));
    }

    @Override
    public Single<List<LiveEventLocation>> getLiveEventLocations() {
        return liveEventLocationDao.getAll()
                .flattenAsObservable(list -> list)
                .map(it -> liveEventLocationMapper.toData(it)).toList();

    }

    @Override
    public Single<Long> saveLiveEventLocation(LiveEventLocation location) {
        return liveEventLocationDao.insert(liveEventLocationMapper.fromData(location))
                .onErrorResumeNext(Single::error);
    }

    @Override
    public Single<List<Long>> saveLiveEventLocations(List<LiveEventLocation> locations) {
        List<LiveEventLocationEntity> entities = new ArrayList<>();
        for (LiveEventLocation n : locations) {
            entities.add(liveEventLocationMapper.fromData(n));
        }
        return liveEventLocationDao.insert(entities).onErrorResumeNext(Single::error);
    }

}
