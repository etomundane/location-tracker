package com.sebangsa.mapstesting.data.model.usecase;

import android.location.Location;

import com.sebangsa.mapstesting.data.base.UseCase;
import com.sebangsa.mapstesting.data.model.Constant;
import com.sebangsa.mapstesting.data.repository.userlocation.UserLocationRepository;

import javax.inject.Inject;

public class CheckUserMoving implements UseCase<Boolean, Location> {
    private UserLocationRepository repository;

    @Inject
    public CheckUserMoving(UserLocationRepository repository) {
        this.repository = repository;
    }

    @Override
    public Boolean execute(Location checkLocation) {
        Location lastSavedLocation = repository.getLastUserLocation();
        return lastSavedLocation == null || lastSavedLocation.distanceTo(checkLocation) > Constant.MIN_USER_MOVING_DISTANCE;
    }
}
