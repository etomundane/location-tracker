package com.sebangsa.mapstesting.data.remote.source;

import android.location.Location;

import com.sebangsa.mapstesting.data.remote.api.EventApi;
import com.sebangsa.mapstesting.data.remote.api.response.LiveEventLocationResponse;
import com.sebangsa.mapstesting.data.mapper.LiveEventLocationResponseMapper;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;
import com.sebangsa.mapstesting.data.repository.liveevent.LiveEventSource;
import com.sebangsa.mapstesting.di.qualifier.DummyLiveEvents;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.functions.Function;

public class RemoteLiveEvent implements LiveEventSource {
    private LiveEventLocationResponseMapper mapper;
    private EventApi eventApi;
    private List<LiveEventLocation> dummyLocations;

    @Inject
    public RemoteLiveEvent(EventApi eventApi, LiveEventLocationResponseMapper mapper, /* TODO */ @DummyLiveEvents List<LiveEventLocation> dummyLocations) {
        this.eventApi = eventApi;
        this.mapper = mapper;
        this.dummyLocations = dummyLocations;
    }

    @Override
    public Observable<LiveEventLocation> getLiveEventLocation(long id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<List<LiveEventLocation>> getLiveEventLocations() {
//        return eventApi.getLiveEventLocations(null)
//                .onErrorResumeNext((Function<Throwable, ObservableSource<? extends List<LiveEventLocationResponse>>>) Observable::error)
//                .flatMapIterable(list -> list)
//                .map(item -> mapper.toData(item))
//                .toList();
        return Single.just(dummyLocations);
    }

    @Override
    public Single<Long> saveLiveEventLocation(LiveEventLocation location) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Single<List<Long>> saveLiveEventLocations(List<LiveEventLocation> locations) {
        throw new UnsupportedOperationException();
    }
}
