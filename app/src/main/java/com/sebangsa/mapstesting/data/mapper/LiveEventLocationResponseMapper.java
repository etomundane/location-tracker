package com.sebangsa.mapstesting.data.mapper;

import com.sebangsa.mapstesting.data.base.DataMapper;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;
import com.sebangsa.mapstesting.data.remote.api.response.LiveEventLocationResponse;

import javax.inject.Inject;

public final class LiveEventLocationResponseMapper implements DataMapper<LiveEventLocationResponse, LiveEventLocation> {

    @Inject
    public LiveEventLocationResponseMapper() {
    }

    @Override
    public LiveEventLocationResponse fromData(LiveEventLocation data) {
        throw new UnsupportedOperationException();
    }

    @Override
    public LiveEventLocation toData(LiveEventLocationResponse entity) {
        LiveEventLocation result = new LiveEventLocation();
        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setLat(entity.getLat());
        result.setLng(entity.getLng());
        result.setRadius(entity.getRadius());
        result.setLastNotified(-1);
        return result;
    }
}
