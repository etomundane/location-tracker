package com.sebangsa.mapstesting.data.local;

import com.sebangsa.mapstesting.data.local.source.LocalFCM;
import com.sebangsa.mapstesting.data.local.source.LocalUserLocation;
import com.sebangsa.mapstesting.data.repository.fcm.FCMSource;
import com.sebangsa.mapstesting.data.repository.liveevent.LiveEventSource;
import com.sebangsa.mapstesting.data.local.source.LocalLiveEvent;
import com.sebangsa.mapstesting.data.repository.userlocation.UserLocationSource;
import com.sebangsa.mapstesting.di.qualifier.LocalData;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class LocalDataSourceModule {
    @Binds
    @LocalData
    abstract FCMSource fcmSource(LocalFCM localFCM);

    @Binds
    @LocalData
    abstract LiveEventSource liveEventSource(LocalLiveEvent localLiveEvent);

    @Binds
    @LocalData
    abstract UserLocationSource userLocationSource(LocalUserLocation localUserLocation);
}
