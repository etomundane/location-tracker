package com.sebangsa.mapstesting.data.repository.fcm;

import com.sebangsa.mapstesting.data.base.BaseRepository;
import com.sebangsa.mapstesting.di.qualifier.LocalData;
import com.sebangsa.mapstesting.di.qualifier.RemoteData;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;

public class FCMRepoFactory extends BaseRepository<FCMSource> implements FCMRepository {

    @Inject
    public FCMRepoFactory(@LocalData FCMSource localData, @RemoteData FCMSource remoteData) {
        super(localData, remoteData);
    }

    @Override
    public Completable persistToken(String token) {
        return getLocalData().saveLocalToken(token);
    }

    @Override
    public Observable<String> updateTokenForUser(long uid, String token) {
        return null;
    }

    @Override
    public Observable<String> getFcmToken() {
        return getLocalData().getToken();
    }
}
