package com.sebangsa.mapstesting.data.local.database.dao;

import com.sebangsa.mapstesting.data.base.BaseDao;
import com.sebangsa.mapstesting.data.local.database.entity.LiveEventLocationEntity;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Query;
import io.reactivex.Observable;
import io.reactivex.Single;

@Dao
public interface LiveEventLocationDao extends BaseDao<LiveEventLocationEntity> {
    @Query("select * from " + LiveEventLocationEntity.TABLE_NAME)
    Single<List<LiveEventLocationEntity>> getAll();

    @Query("select * from " + LiveEventLocationEntity.TABLE_NAME + " where " + LiveEventLocationEntity.C_ID + "=:id")
    Observable<LiveEventLocationEntity> get(long id);

    @Query("delete from  " + LiveEventLocationEntity.TABLE_NAME)
    int clearTable();
}
