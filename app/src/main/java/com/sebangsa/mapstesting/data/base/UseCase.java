package com.sebangsa.mapstesting.data.base;

public interface UseCase<RESULT, PARAM> {
    RESULT execute(PARAM param);
}
