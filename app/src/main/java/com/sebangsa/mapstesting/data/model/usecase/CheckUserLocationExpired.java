package com.sebangsa.mapstesting.data.model.usecase;

import com.sebangsa.mapstesting.data.base.UseCase;
import com.sebangsa.mapstesting.data.model.Constant;
import com.sebangsa.mapstesting.data.repository.userlocation.UserLocationRepository;

import javax.inject.Inject;

import timber.log.Timber;

public class CheckUserLocationExpired implements UseCase<Boolean, Void> {
    private UserLocationRepository repository;

    @Inject
    public CheckUserLocationExpired(UserLocationRepository repository) {
        this.repository = repository;
    }

    @Override
    public Boolean execute(Void aVoid) {
        long lastSavedTime = repository.getLastLocationSavedTime();
        boolean updateIntervalExpired = lastSavedTime <= 0 || System.currentTimeMillis() - lastSavedTime > Constant.MAX_LOCATION_CHECKING_INTERVAL;
        return repository.getLastUserLocation() == null || updateIntervalExpired;
    }
}
