package com.sebangsa.mapstesting.data.model.usecase;

import android.location.Location;

import com.sebangsa.mapstesting.data.base.UseCase;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;
import com.sebangsa.mapstesting.data.repository.liveevent.LiveEventRepository;
import com.sebangsa.mapstesting.data.repository.userlocation.UserLocationRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class GetNearbyLocations implements UseCase<Single<List<LiveEventLocation>>, Void> {
    private LiveEventRepository liveEventRepository;
    private UserLocationRepository userLocationRepository;

    @Inject
    public GetNearbyLocations(LiveEventRepository liveEventRepository, UserLocationRepository userLocationRepository) {
        this.liveEventRepository = liveEventRepository;
        this.userLocationRepository = userLocationRepository;
    }

    @Override
    public Single<List<LiveEventLocation>> execute(Void aVoid) {
        Location userLocation = userLocationRepository.getLastUserLocation();
        return liveEventRepository.getAllLiveEventLocation()
                .flatMapIterable(list -> list)
                .filter(it -> it.asAndroidLocation().distanceTo(userLocation) < it.getRadius())
                .toList();
    }
}
