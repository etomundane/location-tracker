package com.sebangsa.mapstesting.data.repository.liveevent;


import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface LiveEventSource {
    Observable<LiveEventLocation> getLiveEventLocation(long id);

    Single<List<LiveEventLocation>> getLiveEventLocations();

    Single<Long> saveLiveEventLocation(LiveEventLocation location);

    Single<List<Long>> saveLiveEventLocations(List<LiveEventLocation> locations);
}
