package com.sebangsa.mapstesting.data.repository.fcm;

import io.reactivex.Completable;
import io.reactivex.Observable;

public interface FCMSource {
    Observable<String> updateRemoteToken(long uid, String token);

    Completable saveLocalToken(String token);

    Observable<String> getToken();

}
