package com.sebangsa.mapstesting.data.local.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(tableName = LiveEventLocationEntity.TABLE_NAME)
public final class LiveEventLocationEntity {
    public static final String TABLE_NAME = "live_event_location";
    public static final String C_ID = "id";

    @ColumnInfo(name = LiveEventLocationEntity.C_ID)
    @PrimaryKey
    private long id;
    private String name;
    private double lat;
    private double lng;
    private double radius;
    private long lastNotified;
}
