package com.sebangsa.mapstesting.data.local.preference;

import android.content.Context;
import android.location.Location;

import com.sebangsa.mapstesting.data.base.BasePreference;
import com.sebangsa.mapstesting.di.qualifier.ApplicationContext;
import com.sebangsa.mapstesting.di.qualifier.ApplicationScope;

@ApplicationScope
public abstract class AppPreference extends BasePreference {
    private static final String PREF_NAME = "hm3WRzK4CUI2RmXo8dw3meo2JviUMH3AHxcwT0Hkhz5UMCZ2sLthAYjqiYRfmXXJ";

    protected AppPreference(@ApplicationContext Context context) {
        super(context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE));
    }

    public interface LocationPreference {
        Location getLastKnownLocation();

        void setLastKnownLocation(double lat, double lng);

        long getLastSavedTime();
    }

    public interface FCMPreference {
        String getFCMToken();

        void setFireBaseToken(String token);
    }

}
