package com.sebangsa.mapstesting.data.remote;

import com.sebangsa.mapstesting.data.remote.api.EventApi;
import com.sebangsa.mapstesting.di.qualifier.ApplicationScope;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
abstract class ApiModule {

    @ApplicationScope
    @Provides
    static EventApi eventApi(Retrofit retrofit) {
        return retrofit.create(EventApi.class);
    }

}
