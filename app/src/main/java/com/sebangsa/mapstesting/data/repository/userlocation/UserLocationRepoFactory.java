package com.sebangsa.mapstesting.data.repository.userlocation;

import android.location.Location;

import com.sebangsa.mapstesting.data.base.BaseRepository;
import com.sebangsa.mapstesting.di.qualifier.LocalData;

import javax.inject.Inject;

public class UserLocationRepoFactory extends BaseRepository<UserLocationSource> implements UserLocationRepository {

    @Inject
    public UserLocationRepoFactory(@LocalData UserLocationSource localData) {
        super(localData);
    }

    @Override
    public Location getLastUserLocation() {
        return getLocalData().getLastUserLocation();
    }

    @Override
    public void setUserLocation(double lat, double lng) {
        getLocalData().setUserLocation(lat, lng);
    }

    @Override
    public long getLastLocationSavedTime() {
        return getLocalData().getLastLocationSavedTime();
    }
}
