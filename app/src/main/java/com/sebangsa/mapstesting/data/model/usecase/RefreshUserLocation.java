package com.sebangsa.mapstesting.data.model.usecase;

import android.location.Location;

import com.sebangsa.mapstesting.data.base.UseCase;
import com.sebangsa.mapstesting.data.repository.userlocation.UserLocationRepository;

import javax.inject.Inject;

public class RefreshUserLocation implements UseCase<Void, Location> {
    private UserLocationRepository repository;

    @Inject
    public RefreshUserLocation(UserLocationRepository repository) {
        this.repository = repository;
    }

    @Override
    public Void execute(Location location) {
        repository.setUserLocation(location.getLatitude(), location.getLongitude());
        return null;
    }
}
