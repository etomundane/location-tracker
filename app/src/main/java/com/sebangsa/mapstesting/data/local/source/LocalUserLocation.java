package com.sebangsa.mapstesting.data.local.source;

import android.location.Location;

import com.sebangsa.mapstesting.data.local.preference.AppPreference;
import com.sebangsa.mapstesting.data.repository.userlocation.UserLocationSource;

import javax.inject.Inject;

public class LocalUserLocation implements UserLocationSource {
    private AppPreference.LocationPreference locationPreference;

    @Inject
    public LocalUserLocation(AppPreference.LocationPreference locationPreference) {
        this.locationPreference = locationPreference;
    }

    @Override
    public Location getLastUserLocation() {
        return locationPreference.getLastKnownLocation();
    }

    @Override
    public void setUserLocation(double lat, double lng) {
        locationPreference.setLastKnownLocation(lat, lng);
    }

    @Override
    public long getLastLocationSavedTime() {
        return locationPreference.getLastSavedTime();
    }
}
