package com.sebangsa.mapstesting.service.workermanager;

import android.content.Context;

import androidx.work.OneTimeWorkRequest;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;
import com.sebangsa.mapstesting.di.qualifier.ApplicationContext;
import com.sebangsa.mapstesting.di.qualifier.DummyLiveEvents;
import com.sebangsa.mapstesting.di.qualifier.EventLocationMeasurer;
import com.sebangsa.mapstesting.di.qualifier.UserLocationTracker;
import com.sebangsa.mapstesting.service.locationtracker.LocationMeasurer;
import com.sebangsa.mapstesting.service.locationtracker.LocationTracker;

import java.util.ArrayList;
import java.util.List;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class WorkerManagerModule {

    @Binds
    abstract AppWorker appWorker(AppWorkerFactory appWorkerFactory);

    @Provides
    static FusedLocationProviderClient fusedLocationClient(@ApplicationContext Context context) {
        return LocationServices.getFusedLocationProviderClient(context);
    }

    @Provides
    static LocationRequest locationRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        return locationRequest;
    }

    @Provides
    @UserLocationTracker
    static OneTimeWorkRequest oneTimeLocationTracker() {
        return new OneTimeWorkRequest.Builder(LocationTracker.class)
                .addTag(LocationTracker.TAG)
                .build();
    }

    @Provides
    @EventLocationMeasurer
    static OneTimeWorkRequest oneTimeLocationMeasurer() {
        return new OneTimeWorkRequest.Builder(LocationMeasurer.class)
                .addTag(LocationMeasurer.TAG)
                .build();
    }

    @Provides
    @DummyLiveEvents
    static List<LiveEventLocation> dummyLiveEvents() {
        List<LiveEventLocation> locations = new ArrayList<>();
        locations.add(genDummyLocation(1, "BNI Colombo", -7.745960, 110.388687, 995));
        locations.add(genDummyLocation(2, "PPPPTK Matematika", -7.7523796, 110.3917537, 771));
        locations.add(genDummyLocation(3, "Universitas Gadjah Mada", -7.770887, 110.377611, 2043));
        locations.add(genDummyLocation(4, "UPN Veteran Yogyakarta", -7.76266, 110.4131028, 1000));
        locations.add(genDummyLocation(5, "GOR Kamandanoe", -7.7535219, 110.4410309, 700));
        locations.add(genDummyLocation(6, "KUA Depok", -7.755393, 110.434114, 993));
        locations.add(genDummyLocation(7, "Monumen Yogya Kembali", -7.749615, 110.3690413, 1828));
        return locations;
    }

    private static LiveEventLocation genDummyLocation(long id, String name, double lat, double lng, double radius) {
        LiveEventLocation result = new LiveEventLocation();
        result.setId(id);
        result.setName(name);
        result.setLat(lat);
        result.setLng(lng);
        result.setRadius(radius);
        return result;
    }
}
