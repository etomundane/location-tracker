package com.sebangsa.mapstesting.service.workermanager;

import androidx.lifecycle.LiveData;
import androidx.work.WorkInfo;

public interface AppWorker {

    void startLocationTracker();

    void measureLiveEventLocations();

    LiveData<WorkInfo> getLocationTrackerStatus();

}
