package com.sebangsa.mapstesting.service.workermanager;

import android.content.Context;

import com.sebangsa.mapstesting.di.qualifier.ApplicationContext;
import com.sebangsa.mapstesting.di.qualifier.EventLocationMeasurer;
import com.sebangsa.mapstesting.di.qualifier.UserLocationTracker;
import com.sebangsa.mapstesting.service.locationtracker.LocationMeasurer;
import com.sebangsa.mapstesting.service.locationtracker.LocationTracker;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

public class AppWorkerFactory implements AppWorker {
    private WorkManager workManager;
    private OneTimeWorkRequest locationTracker;
    private OneTimeWorkRequest locationMeasurer;

    @Inject
    public AppWorkerFactory(@ApplicationContext Context context, @UserLocationTracker OneTimeWorkRequest locationTracker, @EventLocationMeasurer OneTimeWorkRequest locationMeasurer) {
        this.workManager = WorkManager.getInstance(context);
        this.locationTracker = locationTracker;
        this.locationMeasurer = locationMeasurer;
    }

    @Override
    public void startLocationTracker() {
        workManager.enqueueUniqueWork(LocationTracker.TAG, ExistingWorkPolicy.KEEP, locationTracker);
    }

    @Override
    public void measureLiveEventLocations() {
        workManager.enqueueUniqueWork(LocationMeasurer.TAG, ExistingWorkPolicy.REPLACE, locationMeasurer);
    }

    @Override
    public LiveData<WorkInfo> getLocationTrackerStatus() {
        return workManager.getWorkInfoByIdLiveData(locationTracker.getId());
    }
}
