package com.sebangsa.mapstesting.service.locationtracker;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.sebangsa.mapstesting.R;
import com.sebangsa.mapstesting.data.model.entity.LiveEventLocation;
import com.sebangsa.mapstesting.data.model.usecase.GetNearbyLocations;
import com.sebangsa.mapstesting.ui.TomatoesActivity;
import com.sebangsa.mapstesting.util.RxScheduler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import dagger.worker.AndroidWorkerInjection;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class LocationMeasurer extends Worker {
    public static final String TAG = LocationMeasurer.class.getSimpleName();

    private Context context;
    private NotificationManagerCompat notificationManager;
    private CompositeDisposable compositeDisposable;

    @Inject
    GetNearbyLocations getNearbyLocations;

    @Inject
    RxScheduler scheduler;

    public LocationMeasurer(Context appContext, WorkerParameters workerParams) {
        super(appContext, workerParams);
        this.context = appContext;
        this.notificationManager = NotificationManagerCompat.from(context);
        this.compositeDisposable = new CompositeDisposable();
    }

    @NonNull
    @Override
    public Result doWork() {
        Timber.e("doWork()");
        AndroidWorkerInjection.inject(this);
        try {
            measureLocation();
            return Result.success();
        } catch (Exception e) {
            e.printStackTrace();
            return Result.failure();
        }
    }

    @Override
    public void onStopped() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        super.onStopped();
    }

    private void measureLocation() {
        Disposable disposable = getNearbyLocations.execute(null)
                .subscribeOn(scheduler.io())
                .subscribe(nearbyLocations -> {
                    if (nearbyLocations.size() > 0) {
                        showNearbyEventsNotification(nearbyLocations);
                    }
                }, Timber::e);
        compositeDisposable.add(disposable);
    }

    private void showNearbyEventsNotification(List<LiveEventLocation> nearbyLiveLocations) {
        if (nearbyLiveLocations.size() > 0) {
            long currentTime = System.currentTimeMillis();
            Intent intent = new Intent(getApplicationContext(), TomatoesActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            String groupKey = "live event";
            String groupTitle = "Nearby Live Event";
            String groupSum = nearbyLiveLocations.size() > 0 ? nearbyLiveLocations.size() + " live events near to you" : "";

            HashMap<String, String> titlesAndTexts = new HashMap<>(); /* used for group notification */
            for (LiveEventLocation l : nearbyLiveLocations) {
                titlesAndTexts.put(l.getName(), l.getLat() + ", " + l.getLng());
                showSingleNotification(intent, l.getName(), l.getLat() + ", " + l.getLng(), (int) l.getId(), groupKey);
                l.setLastNotified(currentTime);
            }
            if (titlesAndTexts.size() <= 1) {
                return;
            }
            showGroupSummaryNotification(groupTitle, groupSum, titlesAndTexts, groupKey);

        }
    }

    private void showSingleNotification(Intent intent,
                                        String title,
                                        String message,
                                        int notificationId,
                                        String groupName) {
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.app_default_notification_channel));
        builder.setContentTitle(title)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationManagerCompat.IMPORTANCE_HIGH)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setGroupSummary(false);

        if (groupName != null && !groupName.isEmpty()) {
            builder.setGroup(groupName);
        }

        notificationManager.notify(notificationId, builder.build());
    }

    private void showGroupSummaryNotification(String groupTitle,
                                              String groupSummaryText,
                                              HashMap<String, String> titleTexts,
                                              String groupName) {

        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle()
                .setBigContentTitle(groupTitle)
                .setSummaryText(groupSummaryText);
        for (Map.Entry<String, String> entry : titleTexts.entrySet()) {
            style.addLine(entry.getValue());
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.app_default_notification_channel));
        builder.setContentTitle(groupTitle)
                .setContentText(groupSummaryText)
                .setStyle(style)
                .setNumber(titleTexts.size())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setGroupSummary(true)
                .setGroup(groupName);
        notificationManager.notify(Math.abs(new Random().nextInt()), builder.build());
    }
}
