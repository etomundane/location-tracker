package com.sebangsa.mapstesting.service.firebase;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sebangsa.mapstesting.data.model.usecase.RefreshFCMToken;
import com.sebangsa.mapstesting.di.qualifier.ApplicationScope;
import com.sebangsa.mapstesting.service.workermanager.AppWorker;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import timber.log.Timber;

@ApplicationScope
public class FireBaseMessaging extends FirebaseMessagingService {

    @Inject
    protected RefreshFCMToken refreshFCMToken;
    @Inject
    protected AppWorker appWorker;

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidInjection.inject(this);
    }

    @Override
    public void onNewToken(@NotNull String token) {
        super.onNewToken(token);
        Timber.d("Fire Token: %s", token);
        refreshFCMToken.execute(token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> payload = remoteMessage.getData();
        if (payload.size() > 0) {
            Timber.d("Fire Payload: %s", payload);
        }
        /* TODO ADD CONDITION WITH PAYLOAD DATA */
        appWorker.measureLiveEventLocations();

    }
}
