package com.sebangsa.mapstesting.service.locationtracker;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Process;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.sebangsa.mapstesting.R;
import com.sebangsa.mapstesting.data.model.usecase.CheckUserLocationExpired;
import com.sebangsa.mapstesting.data.model.usecase.CheckUserMoving;
import com.sebangsa.mapstesting.data.model.usecase.RefreshUserLocation;
import com.sebangsa.mapstesting.service.workermanager.AppWorker;

import javax.inject.Inject;

import dagger.worker.AndroidWorkerInjection;
import timber.log.Timber;

public class LocationTracker extends Worker {
    public static final String TAG = LocationTracker.class.getSimpleName();

    private Context context;
    private ThreadHandler handlerThread;
    private LocationCallback locationCallback;
    private OnSuccessListener<Location> onSuccessListener;

    @Inject
    RefreshUserLocation refreshUserLocation;
    @Inject
    CheckUserLocationExpired checkUserLocationExpired;
    @Inject
    CheckUserMoving checkUserMoving;
    @Inject
    FusedLocationProviderClient fusedLocationProviderClient;
    @Inject
    LocationRequest locationRequest;
    @Inject
    AppWorker appWorker;

    public LocationTracker(Context appContext, WorkerParameters workerParams) {
        super(appContext, workerParams);
        this.context = appContext;
    }

    @NonNull
    @Override
    public Result doWork() {
        Timber.e("doWork()");
        AndroidWorkerInjection.inject(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return Result.failure();
            } else {
                doWorkAllowed();
                return Result.success();
            }
        }

        doWorkAllowed();
        return Result.success();
    }

    private void doWorkAllowed() {
        LocationSettingsRequest.Builder settingBuilder = new LocationSettingsRequest.Builder();
        SettingsClient locationSettingClient = LocationServices.getSettingsClient(context);
        Task<LocationSettingsResponse> reqLocationSettings = locationSettingClient.checkLocationSettings(settingBuilder.build());
        reqLocationSettings.addOnSuccessListener(locationSettingsResponse -> {
            try {
                if (checkUserLocationExpired.execute(null)) {
                    Timber.e("User Location Expired");
                    fusedLocationProviderClient.getLastLocation().addOnSuccessListener(getOnSuccessListener());
                }
                if (handlerThread == null) {
                    handlerThread = new ThreadHandler(LocationTracker.class.getSimpleName());
                }
                if (!handlerThread.isAlive()) {
                    handlerThread.start();
                }
                fusedLocationProviderClient.removeLocationUpdates(getLocationCallback())
                        .addOnCompleteListener(task -> fusedLocationProviderClient.requestLocationUpdates(locationRequest, getLocationCallback(), handlerThread.getLooper()));
            } catch (Exception e) {
                Timber.e(e);
            }
        });
        reqLocationSettings.addOnFailureListener(Timber::e);

    }

    private OnSuccessListener<Location> getOnSuccessListener() {
        if (onSuccessListener == null) {
            onSuccessListener = location -> {
                if (location != null) {
                    onLocationFound(location);
                }
            };
        }
        return onSuccessListener;
    }

    private LocationCallback getLocationCallback() {
        if (locationCallback == null) {
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        return;
                    }
                    for (Location location : locationResult.getLocations()) {
                        if (location != null) {
                            onLocationFound(location);
                        }
                    }
                }
            };
        }
        return locationCallback;
    }

    private void onLocationFound(Location location) {
        Timber.e(location.toString());
        dummyNotification();
        if (checkUserMoving.execute(location)) {
            appWorker.measureLiveEventLocations();
        }
        refreshUserLocation.execute(location);
    }

    private void dummyNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.app_default_notification_channel));
        builder.setContentTitle("Moving")
                .setContentText("Location Changed")
                .setPriority(NotificationManagerCompat.IMPORTANCE_HIGH)
                .setAutoCancel(true)
                .setGroup("dummy")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setGroupSummary(false);

        NotificationManagerCompat.from(context).notify(1, builder.build());
    }

    class ThreadHandler extends HandlerThread {
        Handler handler;

        ThreadHandler(String name) {
            super(name, Process.THREAD_PRIORITY_MORE_FAVORABLE);
        }

        @Override
        protected void onLooperPrepared() {
            Looper looper = getLooper();
            if (looper != null) {
                handler = new Handler(looper);
            }
        }
    }
}
