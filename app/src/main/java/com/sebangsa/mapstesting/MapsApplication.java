package com.sebangsa.mapstesting;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.work.Worker;

import com.google.firebase.messaging.FirebaseMessaging;
import com.sebangsa.mapstesting.di.DaggerAppComponent;
import com.sebangsa.mapstesting.service.workermanager.AppWorker;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.DaggerApplication;
import dagger.worker.HasWorkerInjector;
import timber.log.Timber;

public class MapsApplication extends DaggerApplication implements HasWorkerInjector, LifecycleObserver {
    @Inject
    DispatchingAndroidInjector<Worker> workerDispatchingAndroidInjector;

    @Inject
    protected AppWorker appWorker;

    @Override
    public void onCreate() {
        super.onCreate();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        startLocationTracker();
        createNotificationChannel();
        initFcm();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }

    @Override
    public AndroidInjector<Worker> workerInjector() {
        return workerDispatchingAndroidInjector;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppOnMinimized() {
        startLocationTracker();
    }

    private void startLocationTracker() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                appWorker.startLocationTracker();
            }
        } else {
            appWorker.startLocationTracker();
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = getString(R.string.app_default_notification_channel);
            NotificationChannel channel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void initFcm() {
        FirebaseMessaging.getInstance().subscribeToTopic("live_event").addOnSuccessListener(aVoid -> Timber.d("FCM succeeded subscribes live_event"));
    }
}
